
// 2

db.fruits.aggregate([

    {$match:{$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
    {$count:"itemsLessThan50"}


])
// 3

db.fruits.aggregate([

    {$match:{price:{$lt:30}}},
    {$count: "priceLessThan30"}

])
// 4
db.fruits.aggregate([

    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"$supplier",avgPrice:{$avg:"$price"}}}
])
// 5

db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"$supplier",maxPrice:{$max:"$price"}}}
])
// 6

db.fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"$supplier",minPrice:{$min:"$price"}}}
])
